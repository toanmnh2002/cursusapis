﻿namespace Cursus.Application.Pagination
{
    public class PagedList<T>
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalRow { get; set; }
        public IEnumerable<T> PagedData { get; set; }
        public int PageCount => (int)Math.Ceiling(TotalRow * 1.0 / PageSize);
        public bool HasPrevious => PageIndex > 0;
        public bool HasNext => PageIndex < PageCount - 1;
    }
}
