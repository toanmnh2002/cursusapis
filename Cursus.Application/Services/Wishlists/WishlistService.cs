﻿using Cursus.Application.UnitOfWork;

namespace Cursus.Application.Services.Wishlists
{
    public class WishlistService
    {
        private readonly IUnitOfWork _unitOfWork;

        public WishlistService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
