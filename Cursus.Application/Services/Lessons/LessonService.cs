﻿using Cursus.Application.UnitOfWork;

namespace Cursus.Application.Services.Lessons
{
    public class LessonService
    {
        private readonly IUnitOfWork _unitOfWork;

        public LessonService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
