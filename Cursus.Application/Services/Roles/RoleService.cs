﻿using Cursus.Application.UnitOfWork;

namespace Cursus.Application.Services.Roles
{
    public class RoleService
    {
        private readonly IUnitOfWork _unitOfWork;

        public RoleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
