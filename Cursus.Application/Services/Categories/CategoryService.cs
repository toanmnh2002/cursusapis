﻿using Cursus.Application.DTOs;
using Cursus.Application.UnitOfWork;
using Cursus.Domain.Entities;

namespace Cursus.Application.Services.Categories
{
    public class CategoryService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Category CreateCategory(CategoryCreateDto createDto)
        {
            var category = new Category
            {
                Name = createDto.Name,
                Slug = createDto.Slug
            };
            _unitOfWork.Category.Add(category);
            _unitOfWork.SaveChangesAsync();

            return category;
        }
    }
}
