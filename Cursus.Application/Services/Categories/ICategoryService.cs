﻿using Cursus.Application.DTOs;
using Cursus.Domain.Entities;

namespace Cursus.Application.Services.Categories
{
    public interface ICategoryService
    {
        Task<Category> CreateCategory(CategoryCreateDto createDto);
        Task<CategoryDto> GetCategories(); 
        Task<bool> UpdateCategory(int categoryId, CategoryUpdateDto updateDto);
        Task<bool> DeleteCategory(int categoryId);
    }
}
