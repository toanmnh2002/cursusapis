﻿using Cursus.Application.UnitOfWork;

namespace Cursus.Application.Services.Users
{
    public class UserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
