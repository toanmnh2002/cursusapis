﻿using Cursus.Application.UnitOfWork;

namespace Cursus.Application.Services.Courses
{
    public class CourseService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CourseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
