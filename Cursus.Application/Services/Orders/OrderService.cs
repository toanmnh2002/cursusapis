﻿using Cursus.Application.UnitOfWork;

namespace Cursus.Application.Services.Orders
{
    public class OrderService
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrderService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
