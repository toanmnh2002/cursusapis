﻿namespace Cursus.Application.DTOs;

public class QueryDTO
{
    public int PageNumber { get; set; } = 1;
    public int PageSize { get; set; } = 10;
    public string Keyword { get; set; } = string.Empty;
}
