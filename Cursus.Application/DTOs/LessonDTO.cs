﻿namespace Cursus.Application.DTOs;

public class LessonDTO
{
    public string Title { get; set; }
    public string Content { get; set; }
    public string VideoURL { get; set; }
    public string Slug { get; set; }
}
