﻿namespace Cursus.Application.DTOs
{
    public class CategoryDto
    {
        public string Name { get; set; }
        public string Slug { get; set; }
    }

    public class CategoryCreateDto
    {
        public string Name { get; set; }
        public string Slug { get; set; }
    }

    public class CategoryUpdateDto
    {
        public string Name { get; set; }
        public string Slug { get; set; }
    }
}
