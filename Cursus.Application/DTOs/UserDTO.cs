﻿namespace Cursus.Application.DTOs;

public class UserDTO
{
    public string Username { get; set; }
    public string FullName { get; set; }
    public string PhoneNumber { get; set; }
    public string Email { get; set; }
}
