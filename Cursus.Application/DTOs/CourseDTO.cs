﻿namespace Cursus.Application.DTOs;

public class CourseDto
{
    public int Id { get; set; }
    public string CourseName { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public string Slug { get; set; }
}

public class CourseDetailDTO
{
    public int Id { get; set; }
    public string CourseName { get; set; }
    public string Description { get; set; }
    public decimal Price { get; set; }
    public string Slug { get; set; }

    public UserDTO Instructor { get; set; }
    public List<LessonDTO> Lessons {  get; set; }
    public List<CategoryDto> Categories { get; set; }
    public List<UserDTO> Students {  get; set; }
}