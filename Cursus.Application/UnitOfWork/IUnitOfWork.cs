﻿using Cursus.Domain.Repositories;

namespace Cursus.Application.UnitOfWork
{
    public interface IUnitOfWork
    {
        ICategoryRepository Category { get; }
        ICourseRepository Course { get; }
        ILessonRepository Lesson { get; }
        IOrderRepository Order { get; }
        IRoleRepository Role { get; }
        IUserRepository User { get; }
        IWishlistRepository Wishlist { get; }
        Task SaveChangesAsync();
        Task BeginTransactionAsync();
        Task CommitTransactionAsync();
        Task RollbackTransactionAsync();
    }
}