﻿using Cursus.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Cursus.Infrastructure.FluentAPIs
{
    public class Configurations
    {
        public class BaseConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : EntityBase
        {
            public virtual void Configure(EntityTypeBuilder<TEntity> builder)
            {
                builder.HasKey(x => x.Id);
            }
        }

        public class UserConfiguration : BaseConfiguration<User>
        {
            public override void Configure(EntityTypeBuilder<User> builder)
            {
                base.Configure(builder);


                builder.HasIndex(x => x.Email).IsUnique();
                builder.HasIndex(x => x.Username).IsUnique();
                builder.HasIndex(x => x.PhoneNumber).IsUnique();

                builder.Property(x => x.Username).HasMaxLength(50).IsRequired();
                builder.Property(x => x.FullName).HasMaxLength(100).IsRequired();
                builder.Property(x => x.PhoneNumber).HasMaxLength(10);
                builder.Property(x => x.Email).HasMaxLength(100).IsRequired();

            }
        }

        public class UserCourseConfiguration : BaseConfiguration<UserCourse>
        {
            public override void Configure(EntityTypeBuilder<UserCourse> builder)
            {
                base.Configure(builder);
                builder.HasKey(x => new { x.UserId, x.CourseId });
                builder.Ignore(x => x.Id);
                builder.HasOne(x => x.User)
                    .WithMany(x => x.UserCourses)
                    .HasForeignKey(x => x.UserId);

                builder.HasOne(x => x.Course)
                    .WithMany(x => x.UserCourses)
                    .HasForeignKey(x => x.CourseId);
            }
        }

        public class WishListConfiguration : BaseConfiguration<Wishlist>
        {
            public override void Configure(EntityTypeBuilder<Wishlist> builder)
            {
                base.Configure(builder);
                builder.HasKey(x => new { x.UserId, x.CourseId });
                builder.Ignore(x => x.Id);
                builder.HasOne(x => x.User)
                    .WithMany(x => x.Wishlists)
                    .HasForeignKey(x => x.UserId);

                builder.HasOne(x => x.Course)
                    .WithMany(x => x.Wishlists)
                    .HasForeignKey(x => x.CourseId);
            }
        }
        public class EnrollMentConfiguration : BaseConfiguration<Enrollment>
        {
            //public override void Configure(EntityTypeBuilder<Enrollment> builder)
            //{
            //    base.Configure(builder);
            //    builder.HasKey(x => new { x.Username, x.Order });
            //    builder.Ignore(x => x.Id);
            //    builder.HasOne(x => x.User)
            //        .WithMany(x => x.Enrollments)
            //        .HasForeignKey(x => x.UserId);

            //    builder.HasOne(x => x.Course)
            //        .WithMany(x => x.Enrollments)
            //        .HasForeignKey(x => x.CourseId);
            //}
        }

    }
}
