﻿using Cursus.Domain.Entities;
using Cursus.Infrastructure.Data;
using Cursus.Infrastructure.Repositories;

namespace Cursus.Domain.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(AppDbContext context) : base(context)
        {
        }
    }
}
