﻿using Cursus.Domain.Entities;
using Cursus.Infrastructure.Data;
using Cursus.Infrastructure.Repositories;

namespace Cursus.Domain.Repositories
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(AppDbContext context) : base(context)
        {
        }
    }
}
