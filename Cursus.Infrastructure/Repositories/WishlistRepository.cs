﻿using Cursus.Domain.Entities;
using Cursus.Infrastructure.Data;
using Cursus.Infrastructure.Repositories;

namespace Cursus.Domain.Repositories
{
    public class WishlistRepository : RepositoryBase<Wishlist>, IWishlistRepository
    {
        public WishlistRepository(AppDbContext context) : base(context)
        {
        }
    }
}
