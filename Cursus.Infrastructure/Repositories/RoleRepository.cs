﻿using Cursus.Domain.Entities;
using Cursus.Infrastructure.Data;
using Cursus.Infrastructure.Repositories;

namespace Cursus.Domain.Repositories
{
    public class RoleRepository : RepositoryBase<Role>, IRoleRepository
    {
        public RoleRepository(AppDbContext context) : base(context)
        {
        }
    }
}
