﻿using Cursus.Domain.Entities;
using Cursus.Infrastructure.Data;
using Cursus.Infrastructure.Repositories;

namespace Cursus.Domain.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext context) : base(context)
        {
        }
    }
}
