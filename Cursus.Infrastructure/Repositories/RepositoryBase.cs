﻿using Cursus.Domain.Repositories;
using Cursus.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Cursus.Infrastructure.Repositories
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected readonly AppDbContext _context;
        protected RepositoryBase(AppDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<T>> GetAll(int pageSize, int pageIndex)
        {
            DbSet<T> query = _context.Set<T>();
            int totalRows = await query.CountAsync();
            return query.Skip(pageIndex * pageSize).Take(pageSize).AsNoTracking().AsEnumerable();
        }

        public Task<T> GetEntityByCondition(
            Expression<Func<T, bool>> predicate,
            bool trackChanges = false)
        {
            return trackChanges switch
            {
                true => _context.Set<T>().Where(predicate).FirstOrDefaultAsync(),
                false => _context.Set<T>().Where(predicate).AsNoTracking().FirstOrDefaultAsync()
            };
        }
        public void Add(T entity)
            => _context.Add(entity);


        public void Remove(T entity)
            => _context.Remove(entity);

    }
}
