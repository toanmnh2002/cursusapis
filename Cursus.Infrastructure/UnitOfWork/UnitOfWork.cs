﻿using Cursus.Application.UnitOfWork;
using Cursus.Domain.Repositories;
using Cursus.Infrastructure.Data;
using Microsoft.EntityFrameworkCore.Storage;

namespace Cursus.Infrastructure.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly Lazy<ICategoryRepository> _categoryRepository;
        private readonly Lazy<ICourseRepository> _courseRepository;
        private readonly Lazy<ILessonRepository> _lessonRepository;
        private readonly Lazy<IOrderRepository> _orderRepository;
        private readonly Lazy<IRoleRepository> _roleRepository;
        private readonly Lazy<IUserRepository> _userRepository;
        private readonly Lazy<IWishlistRepository> _wishlistRepository;
        private IDbContextTransaction _transaction;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
            _categoryRepository = new Lazy<ICategoryRepository>(() => new CategoryRepository(context));
            _courseRepository = new Lazy<ICourseRepository>(() => new CourseRepository(context));
            _lessonRepository = new Lazy<ILessonRepository>(() => new LessonRepository(context));
            _orderRepository = new Lazy<IOrderRepository>(() => new OrderRepository(context));
            _roleRepository = new Lazy<IRoleRepository>(() => new RoleRepository(context));
            _userRepository = new Lazy<IUserRepository>(() => new UserRepository(context));
            _wishlistRepository = new Lazy<IWishlistRepository>(() => new WishlistRepository(context));
        }

        public ICategoryRepository Category => _categoryRepository.Value;

        public ICourseRepository Course => _courseRepository.Value;

        public ILessonRepository Lesson => _lessonRepository.Value;

        public IOrderRepository Order => _orderRepository.Value;

        public IRoleRepository Role => _roleRepository.Value;

        public IUserRepository User => _userRepository.Value;

        public IWishlistRepository Wishlist => _wishlistRepository.Value;

        public async Task SaveChangesAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task BeginTransactionAsync()
        {
            _transaction = await _context.Database.BeginTransactionAsync();
        }

        public async Task CommitTransactionAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
                await _transaction.CommitAsync();
            }
            finally
            {
                _transaction.Dispose();
            }
        }

        public async Task RollbackTransactionAsync()
        {
            if (_transaction != null)
            {
                await _transaction.RollbackAsync();
                _transaction.Dispose();
            }
        }
    }
}
