﻿using System.Linq.Expressions;

namespace Cursus.Domain.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        void Add(T entity);
        void Remove(T entity);
        Task<IEnumerable<T>> GetAll(int pageSize, int pageIndex);
        Task<T> GetEntityByCondition(Expression<Func<T, bool>> predicate, bool trackChanges = false);
    }
}