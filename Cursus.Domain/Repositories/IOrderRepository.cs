﻿using Cursus.Domain.Entities;

namespace Cursus.Domain.Repositories
{
    public interface IOrderRepository : IRepositoryBase<Order>
    {
    }
}
