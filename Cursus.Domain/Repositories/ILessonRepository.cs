﻿using Cursus.Domain.Entities;

namespace Cursus.Domain.Repositories
{
    public interface ILessonRepository : IRepositoryBase<Lesson>
    {
    }
}
