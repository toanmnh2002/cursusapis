﻿using Cursus.Domain.Entities;

namespace Cursus.Domain.Repositories
{
    public interface IRoleRepository : IRepositoryBase<Role>
    {
    }
}
