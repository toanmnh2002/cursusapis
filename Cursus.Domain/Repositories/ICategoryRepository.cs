﻿using Cursus.Domain.Entities;

namespace Cursus.Domain.Repositories
{
    public interface ICategoryRepository : IRepositoryBase<Category>
    {
    }
}
