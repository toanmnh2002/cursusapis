﻿namespace Cursus.Domain.Entities
{
    //Inherit this class if the entity use Id (Int) as PK
    public abstract class EntityBase
    {
        public int Id { get; set; }
    }
}
