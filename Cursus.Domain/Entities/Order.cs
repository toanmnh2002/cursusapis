﻿namespace Cursus.Domain.Entities
{
    public class Order : EntityBase
    {
        public DateTime OrderDate { get; set; }
        public decimal TotalAmount { get; set; }
        public bool Status { get; set; }

        public string Username { get; set; }
        public User User { get; set; }

        //Represent for OrderDetails 
        public ICollection<Enrollment> Enrollments { get; set; }
        
    }
}
