﻿namespace Cursus.Domain.Entities
{
    //Intermediate Entity beetween Course and User. User - Course (n - n)
    public class Wishlist : EntityBase
    {
        public int UserId { get; set; }
        public int CourseId { get; set; }
        public User User { get; set; }
        public Course Course { get; set; }
    }
}
