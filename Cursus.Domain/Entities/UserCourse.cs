﻿namespace Cursus.Domain.Entities
{
    //Intermediate entity beetween User & Course (n - n)
    public class UserCourse : EntityBase
    {
        public int UserId { get; set; }
        public int CourseId { get; set; }

        //Need this field to check if user is the Instructor/Teaching Assistant or not.
        public bool IsInstructor { get; set; }
        public User User { get; set; }
        public Course Course { get; set; }
    }
}
