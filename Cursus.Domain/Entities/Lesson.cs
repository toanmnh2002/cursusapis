﻿namespace Cursus.Domain.Entities
{
    public class Lesson : EntityBase
    {
        public string Title { get; set; }
        public string Content { get; set;}
        public string VideoURL { get; set;}
        public string Slug { get; set; }

        //Lesson - Course (1 - 1)
        public int CourseId { get; set;}
        public Course Course { get; set;}

        //Lesson - Lesson Completetion (1 - n)
        public ICollection<LessonCompletion> LessonCompletions { get; set; }
}
}