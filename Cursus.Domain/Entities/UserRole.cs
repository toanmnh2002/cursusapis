﻿namespace Cursus.Domain.Entities
{
    //Intermediate Entity beetween User & Role. User - Role (n - n)
    public class UserRole
    {
        public string Username {  get; set; }
        public User User { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}
