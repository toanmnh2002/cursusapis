﻿namespace Cursus.Domain.Entities
{
    public class Course : EntityBase
    {
        public string CourseName { get; set; }
        public string Description { get; set; }
        public string CourseThumbnailFilePath { get; set; }
        public decimal Price { get; set; }
        public string Slug { get; set; }

        //Course - Instructor (1 - 1). To get the Main Instructor of the Course
        public string Username { get; set; }
        public User Instructor { get; set; }

        //Course - UserCourse (1 - n). To get the Course's Students or Teaching Assistants
        public ICollection<UserCourse> UserCourses { get; set; }

        //Course - Lesson (1 - n). To get all lessons of a course. 
        public ICollection<Lesson> Lessons { get; set; }

        //Course - Category (1 - n). To get all categories of a course.
        public ICollection<CourseCategory> CourseCategories { get; set; }
        public ICollection<Wishlist> Wishlists { get; set; }

    }
}
