﻿namespace Cursus.Domain.Entities
{
    public class User : EntityBase
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        //To verify that account is activated or not
        public bool IsEmailConfirmed { get; set; } = false;
        // Token for verification or password reset
        public string VerificationToken { get; set; }
        // Token expiration time
        public DateTime? TokenExpirationTime { get; set; }

        //User - UserCourse (1 - n). To find All Courses that User enrolled or instructred
        public ICollection<UserCourse> UserCourses { get; set; }

        //User - Wishlist (1 - n)
        public ICollection<Wishlist> Wishlists { get; set; }

        //User - Order (1 - n)
        public ICollection<Order> Orders { get; set; }

        //User - LessonCompletion (1 - n). To track completion of a lesson learn by an user.
        public ICollection<LessonCompletion> LessonCompletions { get; set; }

        //User - Course (1 - n). To find all Courses that user instructed.
        public ICollection<Course> InstructedCourses { get; set; }

        //User - Role (1 - n). To get all role of User
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
