﻿namespace Cursus.Domain.Entities
{
    //Intermediate Entity beetween Course & Category
    public class CourseCategory
    {
        public int CourseId { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public Course Course { get; set; }
    }
}
