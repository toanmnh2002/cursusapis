﻿namespace Cursus.Domain.Entities
{
    public class LessonCompletion
    {
        public bool IsCompleted { get; set; }
        public DateTime CompletionDate { get; set; }
        public string Username { get; set; }
        public User User { get; set; }
        public int LessonId { get; set; }
        public Lesson Lesson { get; set; }
    }
}
