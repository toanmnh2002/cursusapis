﻿namespace Cursus.Domain.Entities
{
    //Intermediate Entity beetween User - Course/User - Order
    //Replacable with OrderDetail. Used for avoiding duplicate perpose. Order - Enrollment (1 - n).
    public class Enrollment : EntityBase
    {
        public DateTime EnrollmentDate { get; set; }
        //Need this prop for save price at the time user purchases.
        public decimal Price { get; set; }

        //Enrollment - Lesson (1 - 1). To get the current lesson that user learn.
        public int LessonId { get; set; }
        public Lesson CurrentLesson { get; set; }

        //Enrollment - User (1 - 1). To get the user who enrolled/purchased the course.
        public int UserId { get; set; }
        public User User { get; set; }

        //Enrollment - User (1 - 1). To get the Course.
        public int CourseId { get; set; }
        public Course Course { get; set; }

        //Enrollment - Order (1 - 1). To get the Order.
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
