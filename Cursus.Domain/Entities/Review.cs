﻿namespace Cursus.Domain.Entities
{
    public class Review : EntityBase
    {
        public int UserId { get; set; }
        public int CourseId { get; set; }
        public decimal Rate { get; set; }
        public string Content { get; set; } = string.Empty;
        public string CreatedAt { get; set; }
        public User User { get; set; }
        public Course Course { get; set; }
    }
}
