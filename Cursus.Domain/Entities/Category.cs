﻿namespace Cursus.Domain.Entities
{
    public class Category : EntityBase
    {
        public string Name { get; set; } = string.Empty;
        public string Slug { get; set; }

        //Category - CourseCategory (1 - n). To get all courses of an category.
        public ICollection<CourseCategory> CourseCategories { get; set;}
    }
}
